import decode from 'jwt-decode';
//import { createBrowserHistory as browserHistory} from 'history';
import auth0 from 'auth0-js';

const ID_TOKEN_KEY = 'id_token';
const ACCESS_TOKEN_KEY = 'access_token';

const CLIENT_ID = 'acme';
const CLIENT_DOMAIN = `api.sample.com/uaa/oauth`;
const REDIRECT = 'http://localhost:3000/callback';
const SCOPE = 'openid';
const AUDIENCE = 'AUDIENCE_ATTRIBUTE';

var auth = new auth0.WebAuth({
  clientID: CLIENT_ID,
  domain: CLIENT_DOMAIN
});

const login = () => {
  
  auth./*popup.*/authorize({
    responseType: 'token id_token',
    redirectUri: REDIRECT,
    //audience: AUDIENCE,
    scope: SCOPE
  }, function(err, authResult) {
    if (err) {
      console.log('error! ' + err);
      return;
    }
  });
  //localStorage.setItem('logged', true);
}

const closePopup = () => {
  auth.popup.callback({});
}

const logout = () => {
  clearIdToken();
  clearAccessToken();
  //localStorage.setItem('logged', false);
}

const requireAuth = (nextState, replace) => {
  alert('1')
  if (!isLoggedIn()) {
    replace({pathname: '/'});
  }
}

export function getIdToken() {
  return localStorage.getItem(ID_TOKEN_KEY);
}

export function getAccessToken() {
  return localStorage.getItem(ACCESS_TOKEN_KEY);
}

function clearIdToken() {
  localStorage.removeItem(ID_TOKEN_KEY);
}

function clearAccessToken() {
  localStorage.removeItem(ACCESS_TOKEN_KEY);
}

// Helper function that will allow us to extract the access_token and id_token
function getParameterByName(name) {
  let match = RegExp('[#&]' + name + '=([^&]*)').exec(window.location.hash);
  return match && decodeURIComponent(match[1].replace(/\+/g, ' '));
}

// Get and store access_token in local storage
export function setAccessToken() {
  let accessToken = getParameterByName('access_token');
  localStorage.setItem(ACCESS_TOKEN_KEY, accessToken);
}

// Get and store id_token in local storage
export function setIdToken() {
  let idToken = getParameterByName('id_token');
  localStorage.setItem(ID_TOKEN_KEY, idToken);
}

const isLoggedIn = () => {
  /*const idToken = getIdToken();
  return !!idToken && !isTokenExpired(idToken)*/;

  return /*localStorage.getItem('logged');//*/!!getAccessToken()
}

function getTokenExpirationDate(encodedToken) {
  const token = decode(encodedToken);
  if (!token.exp) { return null; }

  const date = new Date(0);
  date.setUTCSeconds(token.exp);

  return date;
}

function isTokenExpired(token) {
  const expirationDate = getTokenExpirationDate(token);
  return expirationDate < new Date();
}

export { login, logout, isLoggedIn, requireAuth, closePopup };