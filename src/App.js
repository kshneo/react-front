import React, { Component } from 'react';
import { Switch, Route, Link, withRouter } from 'react-router-dom';
import './App.css';
import Home from './components/Home';
import About from './components/About';
import SideBar from './components/SideBar'
import Callback from './components/Callback';

import AppBar from 'material-ui/AppBar';
import Toolbar from 'material-ui/Toolbar';
import Typography from 'material-ui/Typography';
import Button from 'material-ui/Button';
import Tabs, { Tab } from 'material-ui/Tabs';
import { withStyles } from 'material-ui/styles';

import Dialog from 'material-ui/Dialog';
import Slide from 'material-ui/transitions/Slide';
import CloseIcon from 'material-ui-icons/Close';
import IconButton from 'material-ui/IconButton';

import { login, logout, isLoggedIn, requireAuth } from './service/AuthService';

import { connect } from "react-redux";
import { toggleLoginDrawer } from './js/actions/index';

const styles = (theme) => ({
  root: {
    flexGrow: 1,
  },
  flex: {
    flex: 1,
  },
  appBar: {
    position: 'static',
    backgroundColor: '#607d8b'
  },
  menuButton: {
    marginLeft: -12,
    marginRight: 20,
    color: 'inherit',
    fontSize: theme.typography.fontSize + 2
  },
  label: {
    fontSize: theme.typography.fontSize + 2,
    //[theme.breakpoints.up('md')]: {
    //  fontSize: theme.typography.fontSize + 4,
    //},
  },
  tabRoot: {
    //color:'#3f51b5'
    opacity: 1,
    //backgroundColor: '#f0f0f0'
  },
  loginCenter: {
    display: 'flex',
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'center',
    textAlign: 'center',
    minHeight: '100vh'
  },
  closeButton: {
    color: 'inherit',
    position: 'absolute',
    right: '5px',
    top: '5px'
  },
  hstyle: {
    //flexDirection: 'row',
    //alignSelf: 'flex-start'
    height: '0%'
  }
});

function Transition(props) {
  return <Slide direction="up" {...props} />;
}

const mapStateToProps = (state, ownProps) => ({
  open: state.open
});

const mapDispatchToProps = dispatch => {
  return {
    toggleLoginDrawer: (flag) => dispatch(toggleLoginDrawer(flag))
  };
};

class App extends Component {

  componentDidUpdate(prevProps, prevState) {
    console.log('Component DID UPDATE!')
  }

  render() {

    const { classes } = this.props;

    const tabClasses = { root: classes.tabRoot, label: classes.label };

    return (
      <div className="App">
        <AppBar className={classes.appBar}>
          <Toolbar>
            <Tabs>
              <Tab classes={tabClasses} label="HOME" component={Link} to="/" />
              {
                (isLoggedIn()) ? <Tab classes={tabClasses} label="SERVICES" component={Link} to="/services" /> : ''
              }
            </Tabs>
            <Typography variant="title" color="inherit" className={classes.flex}></Typography>
            {
              !(isLoggedIn()) ?
                <Button className={classes.menuButton} onClick={this.handleClickOpen}>Login</Button> :
                <Button className={classes.menuButton} onClick={this.handleLogout}>Logout</Button>
            }
          </Toolbar>
        </AppBar>

        <Switch>
          <Route exact path='/' component={Home} />
          <Route path='/services' component={SideBar} onEnter={requireAuth} />
          <Route path='/callback' component={Callback} />
        </Switch>

        <Dialog
          fullScreen
          open={this.props.open}
          onClose={this.handleClose}
          transition={Transition}
        >
          <div className={classes.loginCenter}>
            <IconButton className={classes.closeButton} onClick={this.handleClose} aria-label="Close">
              <CloseIcon />
            </IconButton>
            <h2 className={classes.hstyle}>Welcome to Microservices!</h2>
            <h4>Please sign in to your account</h4>
            <Button variant="raised" color="primary" onClick={this.handleLogin}>Continue with KanagatAuth</Button>
          </div>
        </Dialog>
      </div>
    );
  }

  handleLogin = (e) => {
    e.preventDefault();
    login();
  }

  handleLogout = (e) => {
    e.preventDefault();

    console.log(this.props)

    logout();
    this.props.history.push('/');
  }

  handleClickOpen = () => {
    this.props.toggleLoginDrawer(true);
  };

  handleClose = () => {
    this.props.toggleLoginDrawer(false);
  };
}

const AppContainer = connect(
  mapStateToProps,
  mapDispatchToProps
)(App);

export default withRouter(withStyles(styles)(AppContainer));
