import axios from 'axios';
import { getAccessToken } from '../service/AuthService';

const BASE_URL = 'http://192.168.0.181:31142';

function getData(path) {
    const url = BASE_URL + path;

    console.log(`Bearer ${getAccessToken()}`);

    return axios({
        method: 'get',
        url: url,
        headers: {
            'Authorization': `Bearer ${getAccessToken()}`
        }
    }).then(response => response.data, error => console.log(error))
    //.catch(error => console.log(error));
}

export { getData };