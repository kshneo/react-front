import { TOGGLE_LOGIN_DRAWER } from "../constants/action-types";

const initialState = {
    open: false
};

const rootReducer = (state = initialState, action) => {
    switch (action.type) {
        case TOGGLE_LOGIN_DRAWER:
            return Object.assign({}, state, {
                open: action.isDrawerOpen
            });
        default:
            return state;
    }
};

export default rootReducer;