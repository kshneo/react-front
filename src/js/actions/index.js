import { TOGGLE_LOGIN_DRAWER } from "../constants/action-types";

export const toggleLoginDrawer = isDrawerOpen => ({
    type: TOGGLE_LOGIN_DRAWER,
    isDrawerOpen
});