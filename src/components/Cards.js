import React, { Component } from 'react';
import axios from 'axios';
import Table, { TableBody, TableCell, TableHead, TableRow } from 'material-ui/Table';
import Paper from 'material-ui/Paper';
import { CircularProgress } from 'material-ui/Progress';
import { withStyles } from 'material-ui/styles';

import { getData } from './../utils/api';

const CustomTableCell = withStyles(theme => ({
    head: {
        backgroundColor: '#62b2e4',//theme.palette.common.black,
        color: theme.palette.common.white,
        fontSize: 16
    },
    body: {
        fontSize: 14

    },
}))(TableCell);

const styles = theme => ({
    root: {
        width: '100%',
        margin: theme.spacing.unit * 3,
        height: '100%',
        //marginLeft: 30,
        //marginRight: 30,
        //marginBottom: 30,
        overflowX: 'auto',
        overflowY: 'visible'
    },
    table: {
        minWidth: 700
    },
    progress: {
        margin: theme.spacing.unit * 2,
    }
});

class Cards extends Component {

    constructor() {
        super()
        this.state = {
            data: [],
            fetchInProgress: false
        }
    }

    componentDidMount() {
        /*axios.get("http://jsonplaceholder.typicode.com/posts")
            .then((response) => {

                this.setState({ data: response.data })
                console.log(this.state.data);
            })
            .catch(error => console.log(error))
               */

        this.setState({ fetchInProgress: true });

        getData('/card-service/api/cards')
            .then((cards) => {
                console.log(cards)
                this.setState({
                    data: cards,
                    fetchInProgress: false
                })
            }
            )
            .catch(error => {
                this.setState({
                    fetchInProgress: false
                })
            }
            );

        /*this.setState({
            data: JSON.parse(
                `[{
                "id": 1,
                "cardHolderName": "John Warner",
                "pan": "0.80719059471193",
                "validDate": "11/20"
            }, {
                "id": 2,
                "cardHolderName": "Paul Crarte",
                "pan": "0.65853402224056",
                "validDate": "09/25"
            }, {
                "id": 3,
                "cardHolderName": "Ana Hassent",
                "pan": "0.93134700077709",
                "validDate": "01/19"
            }, {
                "id": 4,
                "cardHolderName": "Elena Tarin",
                "pan": "0.00147057782292",
                "validDate": "06/22"
            }, {
                "id": 5,
                "cardHolderName": "Wending Qua",
                "pan": "0.51096538534146",
                "validDate": "03/25"
            }, {
                "id": 6,
                "cardHolderName": "Julio Sanch",
                "pan": "0.94695379813639",
                "validDate": "09/18"
            }, {
                "id": 7,
                "cardHolderName": "Adolf Bianc",
                "pan": "0.33325459127442",
                "validDate": "07/22"
            }]`)
        })*/
    }

    render() {
        const { classes } = this.props;

        return (

            <Paper className={classes.root}>
                {
                    this.state.fetchInProgress ?
                
                    <CircularProgress className={classes.progress} size={50} /> :

                    <Table className={classes.table}>
                        <TableHead>
                            <TableRow>
                                <CustomTableCell>ID</CustomTableCell>
                                <CustomTableCell>CAR HOLDER</CustomTableCell>
                                <CustomTableCell>PAN</CustomTableCell>
                                <CustomTableCell>VALID DATE</CustomTableCell>
                            </TableRow>
                        </TableHead>
                        <TableBody>{this.state.data.map(function (item, key) {

                            return (
                                <TableRow key={key}>
                                    <TableCell width='11'>{item.id}</TableCell>
                                    <TableCell>{item.cardHolderName}</TableCell>
                                    <TableCell>{item.pan}</TableCell>
                                    <TableCell>{item.validDate}</TableCell>
                                </TableRow>
                            )

                        })}</TableBody>
                    </Table>
                }
            </Paper>
        )
    }
}

export default withStyles(styles)(Cards);