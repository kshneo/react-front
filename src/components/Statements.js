import React, { Component } from 'react';
import axios from 'axios';
import Table, { TableBody, TableCell, TableHead, TableRow } from 'material-ui/Table';
import Paper from 'material-ui/Paper';
import { withStyles } from 'material-ui/styles';

import { getData } from './../utils/api';

const CustomTableCell = withStyles(theme => ({
    head: {
        backgroundColor: '#3f51b5',
        color: theme.palette.common.white,
        fontSize: 16
    },
    body: {
        fontSize: 14

    },
}))(TableCell);

const styles = theme => ({
    root: {
        width: '100%',
        margin: theme.spacing.unit * 3,
        height: '100%',
        overflowX: 'auto',
        overflowY: 'visible'
    },
    table: {
        minWidth: 700
    },
});

class Statements extends Component {

    constructor() {
        super()
        this.state = {
            data: []
        }
    }

    componentDidMount() {
        getData('/statement-service/api/statements').then((cards) => {
            console.log(cards)
            this.setState({
                data: cards
            })
        }
        );
    }

    render() {
        const { classes } = this.props;

        return (

            <Paper className={classes.root}>
                <Table className={classes.table}>
                    <TableHead>
                        <TableRow>
                            <CustomTableCell>ID</CustomTableCell>
                            <CustomTableCell>CARD ID</CustomTableCell>
                            <CustomTableCell>OPERATION DATE</CustomTableCell>
                            <CustomTableCell>VALUE</CustomTableCell>
                        </TableRow>
                    </TableHead>
                    <TableBody>{this.state.data.map(function (item, key) {

                        return (
                            <TableRow key={key}>
                                <TableCell width='11'>{item.id}</TableCell>
                                <TableCell>{item.cardId}</TableCell>
                                <TableCell>{item.operationDate}</TableCell>
                                <TableCell>{item.value}</TableCell>
                            </TableRow>
                        )

                    })}</TableBody>
                </Table>
            </Paper>
        )
    }
}

export default withStyles(styles)(Statements);