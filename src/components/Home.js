import React, { Component } from 'react';

class Home extends Component {
  render() {
    return (
        <div>
          <h1 className="App-title">Welcome to Microservices</h1>
        </div>
    );
  }
}

export default Home;