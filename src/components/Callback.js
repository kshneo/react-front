import React, { Component } from 'react';
import { setAccessToken, setIdToken, closePopup } from '../service/AuthService';

class Callback extends Component {

  componentDidMount() {
    setAccessToken();
    setIdToken();
    //closePopup();
    this.props.history.push('/');
  }

  render() {
    return null;
  }
}

export default Callback;