import React, { Component } from 'react';
import Drawer from 'material-ui/Drawer';
import List from 'material-ui/List';
import withStyles from 'material-ui/styles/withStyles';

import { Switch, Route, Link } from 'react-router-dom';

import { ListItem, ListItemIcon, ListItemText } from 'material-ui/List'
import DraftsIcon from 'material-ui-icons/Drafts';
import CreditCardIcon from 'material-ui-icons/CreditCard';
import SendIcon from 'material-ui-icons/Send';

import Cards from './Cards'
import Statements from './Statements'

import { isLoggedIn } from '../service/AuthService'

import Typography from 'material-ui/Typography';

const drawerWidth = 240;

const styles = theme => ({
    root: {
        flexGrow: 1,
        //height: 630,
        zIndex: 1,
        overflow: 'hidden',
        position: 'relative',
        display: 'flex',
        backgroundColor: '#f0f0f0'
    },
    drawerPaper: {
        position: 'relative',
        width: drawerWidth,
        backgroundColor: '#f0f0f0',
        //height: '90vh'
    },
    content: {
        flexGrow: 1,
        backgroundColor: theme.palette.background.default,
        padding: theme.spacing.unit * 3,
        minWidth: 0, // So the Typography noWrap works
    },
    toolbar: theme.mixins.toolbar,
});

class SideBar extends Component {

    componentWillMount() {
        if (!isLoggedIn()) {
            this.props.history.push('/')
        }
    }

    render() {
        const { classes } = this.props;

        return (
            <div className={classes.root}>
                <Drawer
                    variant="permanent"
                    classes={{
                        paper: classes.drawerPaper,
                    }}
                >
                    <List>{mailFolderListItems}</List>
                </Drawer>
                <Switch className={classes.content}>
                    <Route path='/services/cards' component={Cards} />
                    <Route path='/services/statements' component={Statements} />
                </Switch>
            </div>
        );
    }
}

const mailFolderListItems = (
    <div>
        <ListItem button component={Link} to="/services/cards">
            <ListItemIcon>
                <CreditCardIcon />
            </ListItemIcon>
            <ListItemText primary="CARDS" />
        </ListItem>
        <ListItem button component={Link} to="/services/statements">
            <ListItemIcon>
                <SendIcon />
            </ListItemIcon>
            <ListItemText primary="STATEMENTS" />
        </ListItem>
        <ListItem button>
            <ListItemIcon>
                <DraftsIcon />
            </ListItemIcon>
            <ListItemText primary="DEPOSITS" />
        </ListItem>
    </div>
);


export default withStyles(styles)(SideBar);