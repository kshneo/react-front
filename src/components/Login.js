import React, { Component } from 'react';
import Button from 'material-ui/Button';
import TextField from 'material-ui/TextField';
import Dialog from 'material-ui/Dialog';
import Slide from 'material-ui/transitions/Slide';
import CloseIcon from 'material-ui-icons/Close';
import IconButton from 'material-ui/IconButton';

import { login, logout, isLoggedIn, setAccessToken } from '../service/AuthService';

function Transition(props) {
    return <Slide direction="up" {...props} />;
}

class Login extends Component {
    constructor(props) {
        super(props);

        this.state = {
            open: true,
        };

        this.handleFormSubmit = this.handleFormSubmit.bind(this);
    }

    componentWillMount() {
        //alert('login')
    }

    render() {
        const { classes } = this.props;

        return (
            <div>
                <Dialog
                    fullScreen
                    open={this.state.open}
                    onClose={this.handleClose}
                    transition={Transition}
                >
                    <IconButton color="inherit" onClick={this.handleClose} aria-label="Close">
                        <CloseIcon />
                    </IconButton>
                    <TextField
                        hintText="Enter your Username"
                        floatingLabelText="Username"
                    />
                    <br />
                    <TextField
                        type="password"
                        hintText="Enter your Password"
                        floatingLabelText="Password"

                    />
                    <br />
                    <Button variant="raised" color="primary" onClick={/*(event) => this.handleClick(event)*/this.handleFormSubmit}>Submit</Button>
                </Dialog>
            </div>

        );
    }

    handleClickOpen = () => {
        this.setState({ open: true });
    };

    handleClose = () => {
        this.setState({ open: false });
    };

    handleFormSubmit(e) {
        e.preventDefault();

        console.log('object1');
    }
}

export default Login;